package m2dl.corbarmi.asso.client;

import servAsso.AssoServer;

public class AssoClient extends AssoClientGui {

	private final AssoServer assoServer;
	
	public AssoClient(AssoServer server) {
		super();
		this.assoServer = server;
		this.run();
	}

	public void updateResultsTable() {
		// TODO
		// Utiliser le servant Corba pour remplir la table de resultats
		// Pour cela, utiliser clearTable() et addTableItem() de la classe mere
		// getCurrentCP() permet d'acceder au contenu du champ CP
	}

	public static void main(String[] args) {
		// TODO Creer un objet AssoClient
		// Pour cela initialiser une reference Corba vers le serveur, le nom du service étant "AssoServer"
	}
}
